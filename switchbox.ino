
//FOR SYSLOG
//https://github.com/arcao/Syslog
//USE THIS TO CONVERT IMAGES TO BYTE ARRAY
//https://lvgl.io/tools/imageconverter

#include <WiFi.h>
#include <WiFiUdp.h>
#include <Syslog.h>
#include <WebServer.h>

//Just for timer macro
#include <FastLED.h>

//FS
#include "FS.h"
#include "SPIFFS.h"

//For NTP
#include <NTPClient.h>

//Screen
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
//CO2 VOC sensor
#include "SparkFun_SGP30_Arduino_Library.h" 

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
//STATIC IMAGE TO DRAW
static const uint8_t wertteLogo[1024] = {
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x00, 0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xf0, 0x00, 0x0f, 0xff, 0xff, 0xc0, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0x00, 0x07, 0xff, 0xff, 0xff, 0x00, 0xe0, 0x00, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xf8, 0x00, 0x7f, 0xff, 0xff, 0xf8, 0x07, 0xff, 0x00, 0x1f, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xc0, 0x07, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xff, 0xf0, 0x03, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0x00, 0x7f, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xff, 0xfe, 0x00, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xfc, 0x01, 0xff, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xff, 
    0xff, 0xff, 0xf0, 0x0f, 0xff, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x07, 0xff, 0xff, 
    0xff, 0xff, 0xc0, 0x3f, 0xff, 0xff, 0xe0, 0x0f, 0xff, 0xff, 0x0f, 0xff, 0xfe, 0x03, 0xff, 0xff, 
    0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xf8, 0x01, 0xff, 0xff, 0x80, 0xff, 0xff, 
    0xff, 0xfe, 0x03, 0xff, 0xff, 0xf0, 0x07, 0xff, 0xff, 0xe3, 0xfc, 0x7f, 0xff, 0xe0, 0x3f, 0xff, 
    0xff, 0xf8, 0x0f, 0xff, 0xff, 0x80, 0x3f, 0xff, 0xff, 0xe3, 0xfc, 0x7f, 0xff, 0xf0, 0x1f, 0xff, 
    0xff, 0xf0, 0x1f, 0xff, 0xf8, 0x03, 0xff, 0xff, 0xff, 0xe3, 0xfc, 0x7f, 0xff, 0xfc, 0x0f, 0xff, 
    0xff, 0xe0, 0x7f, 0x00, 0xc0, 0x1f, 0xff, 0xff, 0xff, 0x01, 0xf8, 0xff, 0xff, 0xfe, 0x03, 0xff, 
    0xff, 0x80, 0xfc, 0x3c, 0x01, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x01, 0xff, 0xff, 0xfe, 0x01, 0xff, 
    0xff, 0x01, 0xf8, 0xff, 0x0f, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xf0, 0x00, 0xff, 
    0xfe, 0x03, 0xf1, 0xff, 0x1f, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xff, 0xff, 0xff, 0x00, 0xe0, 0x7f, 
    0xfe, 0x07, 0xf8, 0xff, 0x1f, 0xff, 0xff, 0xc0, 0x1f, 0xff, 0xff, 0xff, 0xf8, 0x07, 0xe0, 0x3f, 
    0xfc, 0x0f, 0xfc, 0x3c, 0x3f, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xf0, 0x3f, 
    0xf8, 0x1f, 0xff, 0x00, 0xff, 0xff, 0xe0, 0x0f, 0xff, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xf8, 0x1f, 
    0xf8, 0x1f, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xfc, 0x0f, 
    0xf0, 0x3f, 0xff, 0xff, 0xff, 0xf0, 0x07, 0xff, 0xff, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xfc, 0x0f, 
    0xf0, 0x7f, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xe0, 0x0f, 0xff, 0xff, 0xfe, 0x07, 
    0xe0, 0x7f, 0xff, 0xff, 0xf8, 0x03, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xfe, 0x07, 
    0xe0, 0x7f, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xff, 0xff, 0xf0, 0x07, 0xff, 0xff, 0xff, 0xf8, 0x07, 
    0xe0, 0xff, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xfc, 0x07, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xc0, 0x03, 
    0xc0, 0xff, 0xff, 0xe0, 0x1f, 0xff, 0xff, 0xf0, 0x00, 0x03, 0xff, 0xff, 0xff, 0xfe, 0x01, 0x03, 
    0xc0, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xe3, 0xf8, 0x1f, 0xff, 0xff, 0xff, 0xe0, 0x0f, 0x03, 
    0xc0, 0xff, 0xf0, 0x07, 0xff, 0xff, 0xff, 0xc7, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0x00, 0xff, 0x03, 
    0xc0, 0xff, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xc7, 0xfc, 0x7f, 0xff, 0xff, 0xf0, 0x07, 0xff, 0x03, 
    0xc0, 0xf8, 0x03, 0xff, 0xff, 0xff, 0xfe, 0x01, 0xf0, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xff, 0x03, 
    0xc0, 0xc0, 0x3f, 0xff, 0xff, 0xff, 0xe0, 0x08, 0x03, 0xff, 0xff, 0xf8, 0x03, 0xff, 0xff, 0x03, 
    0xe0, 0x01, 0xff, 0xff, 0xff, 0xff, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xff, 0x03, 
    0xe0, 0x0f, 0xff, 0xff, 0xff, 0xf8, 0x07, 0xff, 0xff, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xff, 0x07, 
    0xe0, 0x7f, 0xff, 0xff, 0xff, 0x80, 0x3f, 0xff, 0xff, 0xff, 0xe0, 0x1f, 0xff, 0xff, 0xfe, 0x07, 
    0xf0, 0x7f, 0xff, 0xff, 0xfc, 0x03, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xfe, 0x07, 
    0xf0, 0x3f, 0xff, 0xff, 0xc0, 0x1f, 0xff, 0xff, 0xff, 0xf0, 0x07, 0xff, 0xff, 0xff, 0xfc, 0x0f, 
    0xf8, 0x3f, 0xff, 0xfe, 0x01, 0xff, 0xff, 0xff, 0xff, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xfc, 0x0f, 
    0xf8, 0x1f, 0xff, 0xe0, 0x0f, 0xff, 0xff, 0xff, 0xf8, 0x03, 0xff, 0xff, 0xe7, 0xff, 0xf8, 0x1f, 
    0xfc, 0x0f, 0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x3f, 0xff, 0xfe, 0x00, 0x7f, 0xf0, 0x3f, 
    0xfe, 0x07, 0xf8, 0x07, 0xff, 0xff, 0xff, 0xfc, 0x01, 0xff, 0xff, 0xfc, 0x7e, 0x1f, 0xf0, 0x3f, 
    0xfe, 0x03, 0x80, 0x7f, 0xff, 0xff, 0xff, 0xe0, 0x1f, 0xff, 0xff, 0xf8, 0xff, 0x1f, 0xe0, 0x7f, 
    0xff, 0x00, 0x03, 0xff, 0xff, 0xf8, 0x1e, 0x00, 0xff, 0xff, 0xff, 0xf8, 0xff, 0x1f, 0xc0, 0xff, 
    0xff, 0x80, 0x1f, 0xff, 0xff, 0xc0, 0x00, 0x07, 0xff, 0xff, 0xff, 0xe0, 0x7f, 0x1f, 0x01, 0xff, 
    0xff, 0xc0, 0x7f, 0xff, 0xff, 0x8f, 0xf0, 0x7f, 0xff, 0xff, 0xfe, 0x00, 0x00, 0x7e, 0x03, 0xff, 
    0xff, 0xf0, 0x3f, 0xff, 0xff, 0x8f, 0xf1, 0xff, 0xff, 0xff, 0xf0, 0x07, 0xc3, 0xfc, 0x07, 0xff, 
    0xff, 0xf8, 0x0f, 0xff, 0xff, 0x8f, 0xf1, 0xff, 0xff, 0xff, 0x00, 0x7f, 0xff, 0xf0, 0x1f, 0xff, 
    0xff, 0xfc, 0x03, 0xff, 0xff, 0xc7, 0xe3, 0xff, 0xff, 0xf8, 0x03, 0xff, 0xff, 0xe0, 0x3f, 0xff, 
    0xff, 0xff, 0x01, 0xff, 0xff, 0xe0, 0x07, 0xff, 0xff, 0x80, 0x3f, 0xff, 0xff, 0x80, 0xff, 0xff, 
    0xff, 0xff, 0xc0, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x01, 0xff, 0xff, 0xfe, 0x01, 0xff, 0xff, 
    0xff, 0xff, 0xe0, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xe0, 0x1f, 0xff, 0xff, 0xf8, 0x07, 0xff, 0xff, 
    0xff, 0xff, 0xf8, 0x03, 0xff, 0xff, 0xff, 0xfe, 0x00, 0xff, 0xff, 0xff, 0xc0, 0x1f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0x00, 0x7f, 0xff, 0xff, 0xf0, 0x0f, 0xff, 0xff, 0xfe, 0x00, 0x7f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xc0, 0x0f, 0xff, 0xff, 0x00, 0x7f, 0xff, 0xff, 0xf0, 0x03, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xf8, 0x00, 0xff, 0xf8, 0x07, 0xff, 0xff, 0xff, 0x00, 0x1f, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0x00, 0x07, 0x80, 0x3f, 0xff, 0xff, 0xe0, 0x00, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xe0, 0x00, 0x01, 0xff, 0xff, 0xf8, 0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
};

//Pins
#define R1PIN 12
#define R2PIN 14

// Syslog server connection info
IPAddress sysIp(192,168,88,200);
#define SYSLOG_PORT 514

// This device info
#define DEVICE_HOSTNAME "SwitchBox"
#define APP_NAME "SwitchBox-App"

//Config port
#define CONFIG_PORT 4200

//Light check defines (See sensor datasheet)
#define L_ON_TRESH 800
#define L_OFF_TRESH 900

//DHT
//#include <DHT.h>
#include "DHTesp.h"

// Set DHT pin:
#define DHTPIN 25
#define DHTTYPE DHT22   // DHT 11 
DHTesp dht; //= DHT(DHTPIN, DHTTYPE);
float curTemp, curHum;

//Intervals for actions
int syslogMsgInterval = 60;
int dhtInterval = 5; //NOTE! Must be atleast 2s

//Screen to show
int screen = 0;
//Button
int prevRead = 1;

//On/Off times (in s)
unsigned long onTime1 = 0;
unsigned long offTime1 = 0;
unsigned long orgOnTime1 = 57600; //Only change these
unsigned long orgOffTime1 = 28800; //Only change these
unsigned long onTime2 = 0;
unsigned long offTime2 = 0;
unsigned long orgOnTime2 = 57600; //Only change these //43200 = 12h
unsigned long orgOffTime2 = 28800; //Only change these
//Prev tick
unsigned long prevTick = 0;
//If forced state act
//Logic:
    //1 = Relay 1 on
    //2 = Relay 2 on
    //3 = Relay 1+2 on
    //0 = None are on
int forcedAct = 0;
//Light sensor check
int lightChk = 0; //0 = not act, 1 = on, 2 = off
int chkTimes = 5; //Max check times
int lVals[50] = {850}; //Init at middle
uint32_t lAvg = 850; //Init at middle
int8_t lIt = 0; //Used in calculating average
boolean lAlarmAct = false; //Alarm activated when switch turns attached light on/off but expected reaction is not seen
//Temp
int tVals[10] = {25}; //Init at middle
float tAvg = 25; //Init at middle
int8_t tIt = 0;
boolean tAlarmAct = false;
int maxTemp = 40; //If over this activate tAlarm
//Hum
int hVals[10] = {30}; //Init at middle
float hAvg = 30; //Init at middle
int8_t hIt = 0;
boolean hAlarmAct = false;
int maxHum = 60; //If over this activate hAlarm
//DHT Error
boolean sAlarmAct = false;
//SGP30 CO2 VOC sensor
uint16_t co2;
uint16_t voc;
SGP30 co2sensor;
boolean co2AlarmAct = false;
boolean vocAlarmAct = false;
boolean s2AlarmAct = false;
int maxCo2 = 800; //ppm
int maxVoc = 100; //ppb
int8_t s2It = 0;
int co2Vals[10] = {400};
int vocVals[10] = {0};
float co2Avg = 400;
float vocAvg = 0;

//Internal error
boolean intErrAct = false;

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udpClient, ntpUdp;
WiFiUDP tstUdp;

//For NTP
IPAddress ntpIp(192, 168, 88, 1);
NTPClient timeClient(ntpUdp, ntpIp, 0, 2000);

// Create a new syslog instance with LOG_KERN facility
Syslog syslog(udpClient, sysIp, SYSLOG_PORT, DEVICE_HOSTNAME, APP_NAME, LOG_KERN);
int iteration = 1;

// WIFI credentials
String WIFI_SSID = "NotUrWifi_RPT";
String WIFI_PASS = "kikkihiiri99";
// Network information
IPAddress ip(192, 168, 88, 180);
// Set gateway to your router's gateway
IPAddress gateway(192, 168, 88, 1);
IPAddress subnet(255, 255, 255, 0);
//Counter for connection attempts
int conAttempts = 0;
//If device is in AP mode (connection failed)
bool apmode = false; //Set to true for debugging                     //SETS DEBUG MODE ACTIVE!! (if set to true)
//AP network info
IPAddress AP_Local_IP(192,168,1,1);
IPAddress AP_Gateway(192,168,1,1);
IPAddress AP_Subnet(255,255,255,0);
//Back up server
WebServer ap_server;
//Normal web server
WebServer w_server;
bool controlPage = true;

//FEATURES
bool ntp = true; //NTP for time
bool sys = true; //Syslog active
bool udp = true; //Use udp for control
bool dhcp = false; //Use dhcp or static IP

//TODO: Switch to using CSV+struct for settings

typedef struct
{
    String appName;
    String hostName;
    String sysAddr;
    String ntpAddr;
    int sysPort;
    int on1;
    int off1;
    int on2;
    int off2;
} switchSettings;

switchSettings actSettings;

//Function declarations to keep compiler happy
void applyConfig(bool defConf = false);
//

void setup() {
    Serial.begin(115200);
    Serial.println("SWITCHBOX ONLINE");
    Serial.println();
    //DHT INIT
    // Setup sensor:
    //dht.begin();
    dht.setup(25, DHTesp::DHT22);

    // Start I2C Communication SDA = 5 and SCL = 4 on Wemos Lolin32 ESP32 with built-in SSD1306 OLED
    Wire.begin(5, 4);
    // Second bus for CO2/VOC sensor
    Wire1.begin(18, 19);
    //Sensor init
    //Initialize sensor
    if (co2sensor.begin(Wire1) == false) 
    {
        Serial.println("No SGP30 Detected. Check connections.");
        s2AlarmAct = true;
    }
    else
    {
        co2sensor.initAirQuality();
    }

    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false)) {
        Serial.println(F("SSD1306 allocation failed"));
        intErrAct = true;
    }
    else
    {
        writeToDisp("ONLINE", 2);
    }
    // Launch SPIFFS file system
    //Change to "true" for 1 flash to format fs in case of mount errors. Also check partition size on board config  
    if(!SPIFFS.begin(false)){ 
        Serial.println("An Error has occurred while mounting SPIFFS");  
        intErrAct = true;
    }
    else
    {
        //switchSettings newSet = {"Swtichbox","Switchbox","192.168.1.1","192.168.1.1",554,200,30,45,500};
        //writeSettings(newSet);

        if(!openReadConfig()) //Read config
        {
            intErrAct = true;
            //Apply default config
            applyDefConfig();
        }
    }
    //Load wifi credentials
    File file = SPIFFS.open("/wifi.ini", "r+"); 
    if(!file){ 
        Serial.println("Failed to open file for reading"); 
        intErrAct = true;
    }
    else //Read wifi config
    {
        String ssid = file.readStringUntil('\r\n');
        String pass = file.readStringUntil('\r\n');
        Serial.println(ssid);
        Serial.println(pass);
        WIFI_SSID = ssid;
        WIFI_PASS = pass;
    }

    //Init the timers
    offTime1 = orgOffTime1 = actSettings.off1;
    onTime1 = orgOnTime1 = actSettings.on1;
    offTime2 = orgOffTime2 = actSettings.off2;
    onTime2 = orgOnTime2 = actSettings.on2;

    //Relay pins
    pinMode(R1PIN, OUTPUT);
    pinMode(R2PIN, OUTPUT);
    //LDR
    pinMode(34, INPUT);
    //Button (Display)
    pinMode(27, INPUT_PULLUP);

    //Check if debug (apmode) should be activated
    int debugCount = 0;
    if(digitalRead(27) == 0)
    {
        while(digitalRead(27) == 0 && debugCount < 10)
        {
            debugCount++;
        }
        if(debugCount >= 10)
        {
            apmode = true;
        }
    }

    if(apmode)
    {
        writeToDisp("DEBUG SETUP...", 2);
        Serial.println("SETTING AP MODE ACTIVE!");
        //Disconnect and set STA off
        WiFi.disconnect(true);
        //Set to AP
        WiFi.mode(WIFI_MODE_AP);
        //Begin
        bool res = WiFi.softAP("SB-RESET-AP");
        Serial.println("AP MODE BEGIN");
        if(!res)
        {
            Serial.println("AP MODE FAILED TO START!");
        }
        else
        {
            //Conf (Conf should be done after start)
            //https://github.com/espressif/arduino-esp32/issues/985
            delay(100);
            WiFi.softAPConfig(AP_Local_IP, AP_Gateway, AP_Subnet);

            //Server begin
            //ap_server = WebServer(80);
            ap_server.on("/", handle_OnConnect);
            ap_server.on("/apply", handle_apply);
            ap_server.onNotFound(handle_NotFound);
            ap_server.begin(80);
            //Show on display
            printDebugCon();
            //DEBUG
            Serial.println("AP MODE ACTIVE!");
            IPAddress IP = WiFi.softAPIP();
            Serial.print("AP IP address: ");
            Serial.println(IP);
            
        }
    }
    else //Skip this part if debug is on
    {
        // We start by connecting to a WiFi network
        Serial.print("Connecting to ");
        Serial.println(WIFI_SSID);
        if(!dhcp)
        {
            WiFi.config(ip, gateway, subnet); //This is needed for RPT Network
        }
        WiFi.mode(WIFI_MODE_STA);
        //NOTE! Conversion from string to char array seems to alter the string
        //Maybe empty space at the end? Try getting rid of arrays
        char ssidArr[WIFI_SSID.length()];
        char pswArr[WIFI_PASS.length()];
        WIFI_SSID.toCharArray(ssidArr, sizeof(ssidArr));
        WIFI_PASS.toCharArray(pswArr, sizeof(pswArr));
        //Temp
        const char* ssid_temp     = "NotUrWifi";
        const char* password_temp = "kikkihiiri99";
        //
        WiFi.begin(ssid_temp, password_temp);
        //WiFi.begin(WIFI_SSID.c_str(), WIFI_PASS.c_str());
        printConInfo(); //Print con info to screen for debug

        while (WiFi.status() != WL_CONNECTED) {
            conAttempts++;
            delay(50);
            Serial.println(".");
            if(conAttempts > 120) //120/2 = 60 so after 1min of no con
            {
                //Set apmode on
                apmode = true;
                break;
            }
            //TODO: Update display as "x/120 attempts"
            //Serial.println(conAttempts);
        }
        //Normal operation
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        // Severity levels can be found in Syslog.h.
        syslog.log(LOG_INFO, "DEVICE START");
        //Listen to UDP port
        tstUdp.begin(CONFIG_PORT);
        Serial.print("Listening on UDP port ");
        Serial.println(CONFIG_PORT);
        //Check if control page is active
        if(controlPage)
        {
            //Server begin
            //w_server = WebServer(80);
            w_server.on("/", handle_OnConnectNorm);
            w_server.on("/reboot", handle_reboot);
            w_server.on("/data", handle_data);
            w_server.on("/force", handle_force);
            w_server.on("/settings", handle_settings);
            w_server.onNotFound(handle_NotFound);
            w_server.begin(80);
            Serial.println("Server begin");
        }
        //Start/get NTP
        timeClient.begin();
        timeClient.update();

        startup();

        
    }

}

void loop() {

    if(apmode)
    {
        ap_server.handleClient();
    }
    else
    {
        w_server.handleClient();
        //Listen for CONFIG
        chkConfMsg();
        //Time loop
        //Trigger once every sec
        if ((millis() - prevTick) > 1000)
        {
            //SYSLOG + TimeUpdate
            EVERY_N_SECONDS(syslogMsgInterval)
            {
                Serial.println("Updating time...");
                timeClient.update();
                //Check for temp alarm
                chkTempAlarm(curTemp);
                //Check for temp alarm
                chkHumAlarm(curHum);
                //Debug prints
                //Serial.println(timeClient.getFormattedTime());
                Serial.println("Sending syslog...");
                char rMsg[200];
                //This creates the reply string
                sprintf(rMsg,"%lu:%lu:%lu:%lu:%lu:%lu:%lu:%lu:%.4g:%.4g:%u", onTime1, onTime2, offTime1, offTime2, orgOnTime1,orgOnTime2,orgOffTime1,orgOffTime2,curHum,curTemp, getLight());
                syslog.log(LOG_INFO, rMsg);
                Serial.println("Syslog sent!");
            }
            EVERY_N_SECONDS(dhtInterval)
            {
                float tmp;
                tmp = getTemp();
                if(tmp > 0)
                {
                    curTemp = tmp;
                }
                tmp = getHum();
                if(tmp > 0)
                {
                    curHum = tmp;
                }
            }
            EVERY_N_SECONDS(1) //NOTE! SPG30 needs to be checked once every second
            {
                //Check co2/voc
                getCo2Voc();
                checkCO2VocAlarm();
            }
            //##
            prevTick = millis();
            //Relay 1
            if (onTime1 > 0 && forcedAct != 1 && forcedAct != 3)
            {
                onTime1 -= 1;
                if (digitalRead(R1PIN) == HIGH);
                else digitalWrite(R1PIN, HIGH);
                if (onTime1 == 0) //Switch to off time
                {
                    offTime1 = orgOffTime1;
                    lightChk = 2;
                }
            }
            else if (offTime1 > 0 && forcedAct != 1 && forcedAct != 3)
            {
                offTime1 -= 1;
                if (digitalRead(R1PIN) == LOW);
                else digitalWrite(R1PIN, LOW);
                if (offTime1 == 0) //Switch to on time
                {
                    onTime1 = orgOnTime1;
                    lightChk = 1;
                }
            }
            //Relay 2
            if (onTime2 > 0 && forcedAct != 2 && forcedAct != 3)
            {
                onTime2 -= 1;
                if (digitalRead(R2PIN) == HIGH);
                else digitalWrite(R2PIN, HIGH);
                if (onTime2 == 0) //Switch to off time
                {
                    offTime2 = orgOffTime2;
                    lightChk = 2;
                }
            }
            else if (offTime2 > 0 && forcedAct != 2 && forcedAct != 3)
            {
                offTime2 -= 1;
                if (digitalRead(R2PIN) == LOW);
                else digitalWrite(R2PIN, LOW);
                if (offTime2 == 0) //Switch to on time
                {
                    onTime2 = orgOnTime2;
                    lightChk = 1;
                }
            }
        }
        //Chk need for screen change
        chkScreenChange();
        //Light check
        lChk();
    }
}
//For webserver
void handle_OnConnect() {
    Serial.println("Handling on connect...");
    File f = SPIFFS.open("/index.html", "r"); 
    ap_server.streamFile(f, "text/html");
    f.close();
}
void handle_OnConnectNorm() {
    Serial.println("Handling on connect...");
    File f = SPIFFS.open("/index2.html", "r"); 
    w_server.streamFile(f, "text/html");
    f.close();
}
void handle_NotFound(){
    ap_server.send(404, "text/plain", "Not found");
    w_server.send(404, "text/plain", "Not found"); //Stupid temp solution
}
void handle_apply()
{
    if(ap_server.args() == 2) //0 = SSID, 1 = psw
    {
        File file = SPIFFS.open("/wifi.ini", "r+"); 
        if(!file){ 
            Serial.println("Failed to open file for reading"); 
            return; 
        }
        else //Write to config
        {
            file.print(ap_server.arg(0));
            file.print("\r\n");
            file.print(ap_server.arg(1));
            file.print("\r\n");
        }
        file.close();
        writeToDisp("Rebooting!", 1);
        ap_server.send(200, "text/plain", "Applying settings....\rRebooting...");
        ESP.restart();
    }
    else
    {
        handle_OnConnect();
    }
}
void handle_reboot()
{
    Serial.println("Rebooting!");
    writeToDisp("Rebooting!", 1);
    w_server.send(200, "text/plain", "Rebooting...");
    syslog.log(LOG_INFO, "Manual reboot from web page");
    ESP.restart();
}
void handle_data()
{
    if(w_server.args() == 1) //0 = Relay num to force
    {
        char rMsg[400];
        if(w_server.arg(0).toInt() == 0) //Init data
        {
            sprintf(rMsg,"{\"ntp\":\"%d\",\"sys\":\"%d\",\"udp\":\"%d\",\"dhcp\":\"%d\",\"sw\":\"%s\",\"ntpServ\":\"%s\",\"sysServ\":\"%s\",\"tLimit\":\"%u\",\"hLimit\":\"%u\"}", ntp, sys, udp, dhcp, "1.0.0", ntpIp.toString().c_str(), sysIp.toString().c_str(), maxTemp, maxHum);
            w_server.send(200, "application/json", String(rMsg));
        }
        if(w_server.arg(0).toInt() == 1) //Params
        {
            sprintf(rMsg,"{\"onTime1\":\"%lu\",\"onTime2\":\"%lu\",\"offTime1\":\"%lu\",\"offTime2\":\"%lu\",\"orgOnTime1\":\"%lu\",\"orgOnTime2\":\"%lu\",\"orgOffTime1\":\"%lu\",\"orgOffTime2\":\"%lu\",\"temp\":\"%.4g\",\"hum\":\"%.4g\",\"ldr\":\"%u\",\"forced\":\"%u\",\"co2\":\"%u\",\"voc\":\"%u\"}", onTime1, onTime2, offTime1, offTime2, orgOnTime1,orgOnTime2,orgOffTime1,orgOffTime2,curTemp,curHum, getLight(), forcedAct, co2, voc);
            w_server.send(200, "application/json", String(rMsg));
        }
        if(w_server.arg(0).toInt() == 2) //Alarms
        {
            sprintf(rMsg,"{\"temp\":\"%d\",\"hum\":\"%d\",\"light\":\"%d\",\"sensor\":\"%d\",\"co2sensor\":\"%d\",\"co2\":\"%d\",\"voc\":\"%d\",\"internal\":\"%d\"}", tAlarmAct, hAlarmAct, lAlarmAct, sAlarmAct, s2AlarmAct, co2AlarmAct, vocAlarmAct, intErrAct);
            w_server.send(200, "application/json", String(rMsg));
        }
    }
    else 
    {
        w_server.send(404, "application/json", "Not found");
    }
}
void handle_force()
{
    if(w_server.args() == 1) //0 = Relay num to force
    {
        if(w_server.arg(0).toInt() == 1)
        {
            //Logic
            //1 = Relay 1 on
            //2 = Relay 2 on
            //3 = Relay 1+2 on
            //0 = None are on
            if(forcedAct == 1)
            {
                forcedAct = 0;
            }
            else if(forcedAct == 2)
            {
                forcedAct = 3;
            }
            else if(forcedAct == 3)
            {
                forcedAct = 2;
            }
            else 
            {
                forcedAct = 1;
            }
        }
        else if(w_server.arg(0).toInt() == 2)
        {
            if(forcedAct == 2)
            {
                forcedAct = 0;
            }
            else if(forcedAct == 1)
            {
                forcedAct = 3;
            }
            else if(forcedAct == 3)
            {
                forcedAct = 1;
            }
            else 
            {
                forcedAct = 2;
            }
        }
        else 
        {
            w_server.send(404, "text/plain", "Relay not found");
        }
        setForced(forcedAct);
        w_server.send(200, "text/plain", "OK");
    }
    else 
    {
        w_server.send(404, "text/plain", "Not found");
    }
}
//Expects query string "type=X" where X denotes settings to be saved
void handle_settings() 
{
    //TODO: Finish this
    if(w_server.arg(0).toInt() == 0) //Syslog (AppName, HostName, Port, IP)
    {
        w_server.send(200, "text/plain", "OK");
    }
    else if(w_server.arg(0).toInt() == 1) //Relay (R1On, R1Off, R2On, R2Off)
    {
        w_server.send(200, "text/plain", "OK");
        //Update struct (for saving)
        actSettings.on1 = w_server.arg(1).toInt();
        actSettings.off1 = w_server.arg(2).toInt();
        actSettings.on2 = w_server.arg(3).toInt();
        actSettings.off2 = w_server.arg(4).toInt();
        //Also update the current timings
        orgOnTime1 = actSettings.on1;
        orgOffTime1 = actSettings.off1;
        orgOnTime2 = actSettings.on2;
        orgOffTime2 = actSettings.off2;
        //Debug
        Serial.println(w_server.arg(1).toInt());
        Serial.println(w_server.arg(2).toInt());
        Serial.println(w_server.arg(3).toInt());
        Serial.println(w_server.arg(4).toInt());
    }
    else if(w_server.arg(0).toInt() == 2) //NTP (IP)
    {
        w_server.send(200, "text/plain", "OK");
    }
    else if(w_server.arg(0).toInt() == 3) //Save settings
    {
        w_server.send(200, "text/plain", "OK");
        writeSettings(actSettings);
        return;
    }
    else 
    {
        w_server.send(404, "text/plain", "Not found");
        return; //Do not save struct if 404
    }
}

//Open and read config (NOTE! has chance for infinite loop)
bool openReadConfig()
{
    
   actSettings = readSettings();
   //If -1 reading failed
   if(actSettings.on1 == -1)
   {
       Serial.println("Read failed!");
       return false;
   }
   else 
   {
       Serial.println("Read OK!");
       return true;
   }
}
//Read settings CSV
//NOTE! Order of variables need to be enforced!
//See struct for correct order
switchSettings readSettings() 
{
    switchSettings ss;
    String settings = getFile("/config.csv");
    Serial.println("Config: ");
    Serial.println(settings);

    int comma = settings.indexOf(",");
    int prevComma = 0;
    ss.appName = settings.substring(0,comma);
    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.hostName = settings.substring(prevComma, comma);
    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.sysAddr = settings.substring(prevComma, comma);
    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.ntpAddr = settings.substring(prevComma, comma);

    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.sysPort = settings.substring(prevComma, comma).toInt();

    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.on1 = settings.substring(prevComma, comma).toInt();

    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.off1 = settings.substring(prevComma, comma).toInt();

    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.on2 = settings.substring(prevComma, comma).toInt();

    prevComma = comma+1;
    comma = settings.indexOf(",", prevComma);
    ss.off2 = settings.substring(prevComma, comma).toInt();
    //Debug
    Serial.println(ss.appName);
    Serial.println(ss.hostName);
    Serial.println(ss.sysAddr);
    Serial.println(ss.ntpAddr);
    Serial.println(ss.sysPort);
    Serial.println(ss.on1);
    Serial.println(ss.off1);
    Serial.println(ss.on2);
    Serial.println(ss.off2);
    return ss;
}
//NOTE! IP addresses seem to get mangled
bool writeSettings(switchSettings s)
{
    File file = SPIFFS.open("/config.csv", "w"); 
    if(!file){ 
        Serial.println("Failed to open file for reading"); 
        return false;
    }
    else //Write file
    {       
        char sett[200];
        sprintf(sett,"%s,%s,%s,%s,%u,%u,%u,%u,%u", s.appName, s.hostName, s.sysAddr, s.ntpAddr, s.sysPort, s.on1, s.off1, s.on2, s.off2);
        file.print(String(sett));
        file.close();
    }
    return true;
}


//Apply default
void applyDefConfig()
{
    switchSettings defSet= {
        "SwitchBox",
        "SwitchBox",
        "192.168.1.1",
        "192.168.1.1",
        554,
        0,
        0,
        0,
        0
    };
    actSettings = defSet;
}

int getLight()
{
    lVals[lIt] = analogRead(34);
    lIt += 1;
    if(lIt == 49) //Prevent overflow
    {
        lAvg = 0;
        for(lIt; lIt >= 0; lIt--)
        {
            lAvg += lVals[lIt];
        }
        lIt = 0;
        lAvg = lAvg/50;
    }
    return lAvg;
}
float getHum()
{
    float tmp = dht.getHumidity();
    if(!isnan(tmp))
    {
        hVals[hIt] = tmp;
        if(hIt == 9) //Prevent overflow
        {
            //Serial.println("Calculating hum avg...");
            hAvg = 0;
            for(hIt; hIt >= 0; hIt--)
            {
                Serial.println(hIt);
                Serial.println(hVals[hIt]);
                hAvg += hVals[hIt];
            }
            hIt = 0;
            hAvg = hAvg/10.0;
            return hAvg;
        }
        hIt += 1;
        sAlarmAct = false;
        return hAvg;
    }
    else 
    {
        sAlarmAct = true;
        Serial.println("HUM IS NAN");
    }
    return -1.0F;
}
float getTemp()
{
    float tmp = dht.getTemperature();
    //Serial.println(tmp);
    if(!isnan(tmp))
    {
        tVals[tIt] = tmp;
        if(tIt == 9) //Prevent overflow
        {
            //Serial.println("Calculating temp avg...");
            tAvg = 0;
            for(tIt; tIt >= 0; tIt--)
            {
                //Serial.println(tIt);
                tAvg += tVals[tIt];
            }
            tIt = 0;
            tAvg = tAvg/10.0;
            return tAvg;
        }
        tIt += 1;
        sAlarmAct = false;
        return tAvg;
    }
    else 
    {
        sAlarmAct = true;
        Serial.println("TEMP IS NAN");
    }
    return -1.0F;
}

void getCo2Voc()
{
    //measure CO2 and TVOC levels
    co2sensor.measureAirQuality();

    co2Vals[s2It] = co2sensor.CO2;
    vocVals[s2It] = co2sensor.TVOC;
    if(s2It == 9) //Prevent overflow
    {
        co2Avg = 0;
        vocAvg = 0;
        for(s2It; s2It >= 0; s2It--)
        {
            //Serial.println(tIt);
            co2Avg += co2Vals[s2It];
            vocAvg += vocVals[s2It];
        }
        s2It = 0;
        co2Avg = co2Avg/10.0;
        vocAvg = vocAvg/10.0;
        co2 = co2Avg;
        voc = vocAvg;
    }
    s2It += 1;

    //co2 = co2sensor.CO2;
    //voc = co2sensor.TVOC;
}

void chkConfMsg()
{
    // If packet received...
    int packetSize = tstUdp.parsePacket();
    if (packetSize) {
        Serial.print("Received packet! Size: ");
        Serial.println(packetSize);
        //Serial.println(packet);
        char packet[300];
        int len = tstUdp.read(packet, 255);
        if (len > 0)
        {
            packet[len] = '\0'; //Isnt this supposed to be +1 to add it to the end? (Though +1 add "?" character to print out)
        }
        //Set time
        if (packet[0] == 't' || packet[0] == 'T') 
        {
            Serial.println("TIME MSG");
            String packetVals[4];
            int i = 0;
            //Maybe this will work since arrays clearly dont
            char* d = strtok(packet, ":");
            while (d != NULL) 
            {
                Serial.println(d);
                if(strlen(d) < 4) //<4 since IDENTIFIER may be 3 characters long
                {
                    Serial.print("IDENTIFIER: ");
                    Serial.println(d);
                }
                else 
                {
                    packetVals[i] = String(d);
                    i++;
                }
                d = strtok(NULL, ":");
            }
            Serial.println("VALUES: ");
            for(int i = 0; i < 4; i++)
            {
                Serial.println(packetVals[i]);
            }
            orgOnTime1 = packetVals[0].toInt();
            orgOffTime1 = packetVals[1].toInt();
            orgOnTime2 = packetVals[2].toInt();
            orgOffTime2 = packetVals[3].toInt();
            //TODO: Add ntp+syslog to config msg
            forcedAct = 0; //Reset forced
            setForced(forcedAct);
            Serial.println("Writing to config...");
            //DEPRECATED FOR STRUCTS
            //writeConfig();
        }
        else if (packet[0] == 'f' || packet[0] == 'F') 
        {
            Serial.println("FORCE MSG");
            String packetVals[2];
            int i = 0;
            //Maybe this will work since arrays clearly dont
            char* d = strtok(packet, ":");
            while (d != NULL) 
            {
                Serial.println(d);
                if(strlen(d) < 4) //<4 since IDENTIFIER may be 3 characters long
                {
                    Serial.print("IDENTIFIER: ");
                    Serial.println(d);
                }
                else 
                {
                    packetVals[i] = String(d);
                    i++;
                }
                d = strtok(NULL, ":");
            }
            Serial.println("FORCED VALUES: ");
            for(int i = 0; i < 2; i++)
            {
                Serial.println(packetVals[i]);
            }
            if(packetVals[0].toInt() > 0) //Switch 1 forced on
            {
                orgOnTime1 = 1;
                orgOffTime1 = 0;
                digitalWrite(R1PIN, HIGH);
            }
            else 
            {
                orgOnTime1 = 0;
                orgOffTime1 = 1;
                digitalWrite(R1PIN, LOW);
            }
            if(packetVals[1].toInt() > 0) //Switch 2 forced on
            {
                orgOnTime2 = 1;
                orgOffTime2 = 0;
                digitalWrite(R2PIN, HIGH);
            }
            else 
            {
                orgOnTime2 = 0;
                orgOffTime2 = 1;
                digitalWrite(R2PIN, LOW);
            }
            forcedAct = 3; //Set forced on
            setForced(forcedAct);
        }
        else if (packet[0] == 'r' || packet[0] == 'R') 
        {
            Serial.println("Report MSG");
            String packetVals[2];
            int i = 0;
            char* d = strtok(packet, ":");
            while (d != NULL) 
            {
                Serial.println(d);
                if(strlen(d) < 4) //<4 since IDENTIFIER may be 3 characters long
                {
                    Serial.print("IDENTIFIER: ");
                    Serial.println(d);
                }
                else 
                {
                    packetVals[i] = String(d);
                    i++;
                }
                d = strtok(NULL, ":");
            }
            WiFiUDP Udp;
            IPAddress replyTarg;
            replyTarg.fromString(packetVals[0]);
            Udp.beginPacket(replyTarg, packetVals[1].toInt());
            
            char rMsg[200];
            //This creates the reply string
            sprintf(rMsg,"%lu:%lu:%lu:%lu:%f:%f:%u",orgOnTime1,orgOnTime2,orgOffTime1,orgOffTime2,curHum,curTemp, getLight());
            int r = 0;
            //Need to send it this way
            while (rMsg[r] != 0) 
            {
                Udp.write((uint8_t)rMsg[r++]);
            }
            Udp.endPacket();
            Serial.println("SEND REPLY");
        }
    }
}
void chkScreenChange()
{
    if(prevRead == 0 && digitalRead(27) == 0)
    {
        screen++;
        if(screen > 3)
        {
            screen = 0;
        }
        if(screen == 0)
        {
            printStatus(true);
        }
        else if(screen == 1)
        {
            printLDR(true);
        }
        else if(screen == 2)
        {
            printConInfo();
        }
        else if(screen == 3)
        {
            //OFF
            dispOff();
        }
    }
    else if(digitalRead(27) == 0)
    {
        prevRead = 0;
    }
    else 
    {
        prevRead = 1;
    }
    if(screen == 0) //Keep this here to refresh counters
    {
        printStatus(false);
    }
    else if(screen == 1)
    {
        printLDR(false);
    }
}
//Check if temperature alarm needs to be activated
void chkTempAlarm(float temp)
{
    if(temp > maxTemp)
    {
        tAlarmAct = true;
    }
    else if(tAlarmAct)
    {
        tAlarmAct = false;
    }
}
//Check if humidity alarm needs to be activated
void chkHumAlarm(float hum)
{
    if(hum > maxHum)
    {
        hAlarmAct = true;
    }
    else if(hAlarmAct)
    {
        hAlarmAct = false;
    }
}
//Check VOC CO2 alarms
void checkCO2VocAlarm()
{
    if(co2 > maxCo2)
    {
        co2AlarmAct = true;
    }
    else 
    {
        co2AlarmAct = false;
    }
    if(voc > maxVoc)
    {
        vocAlarmAct = true;
    }
    else 
    {
        vocAlarmAct = false;
    }
}
//TODO: Fix bug on state 2. Seems to hang up
void lChk()
{
    EVERY_N_SECONDS(5)
    {
        Serial.println("Checking light...");
        Serial.println(lightChk);
        if(lightChk == 1) //Check for on
        {
            Serial.println("Getting light:");
            Serial.println(getLight());
            Serial.println("L_ON_TRESH:");
            Serial.println(L_ON_TRESH);
            Serial.println("CheckTimes:");
            Serial.println(chkTimes);
            if(getLight() > L_ON_TRESH && chkTimes <= 0) //Light NOT on and check time expired
            {
                //Send syslog alarm
                char rMsg[200];
                //This creates the reply string
                sprintf(rMsg,"LIGHT HAS NOT TURNED ON! %.4g:%.4g:%u:%lu", curHum,curTemp, getLight(), timeClient.getEpochTime());
                syslog.log(LOG_ALERT, rMsg);
                //Reset to avoid spam
                chkTimes = 5;
                lightChk = 0;
            }
            else if(getLight() < L_ON_TRESH) //All good
            {
                chkTimes = 5;
                lightChk = 0;
            }
            chkTimes -= 1;
        }
        else if(lightChk == 2) //Check for off (May cause hangups?)
        {
            Serial.println("Getting light:");
            Serial.println(getLight());
            Serial.println("L_OFF_TRESH:");
            Serial.println(L_OFF_TRESH);
            Serial.println("CheckTimes:");
            Serial.println(chkTimes);
            if(getLight() < L_OFF_TRESH && chkTimes <= 0)
            {
                //Send syslog alarm
                char rMsg[200];
                //This creates the reply string
                sprintf(rMsg,"LIGHT HAS NOT TURNED OFF! %.4g:%.4g:%u:%lu", curHum,curTemp, getLight(), timeClient.getEpochTime());
                syslog.log(LOG_ALERT, rMsg);
                //Reset to avoid spam
                chkTimes = 5;
                lightChk = 0;
            }
            else if(getLight() > L_OFF_TRESH) //All good
            {
                chkTimes = 5;
                lightChk = 0;
            }
            chkTimes -= 1;
        }
        //Serial.println("Check Done!");
    }
}
//Set forced state on relay
void setForced(int f)
{
    if(f == 1)
    {
        digitalWrite(R1PIN, HIGH);
        digitalWrite(R2PIN, LOW);
    }
    else if(f == 2)
    {
        digitalWrite(R1PIN, LOW);
        digitalWrite(R2PIN, HIGH);
    }
    else if(f == 3)
    {
        digitalWrite(R1PIN, HIGH);
        digitalWrite(R2PIN, HIGH);
    }
    
    
}
//Write message with font size to display
void writeToDisp(String msg, int size)
{
    display.clearDisplay();
    display.setTextSize(size);
    display.setTextColor(WHITE);
    display.setCursor(0, 10);
    display.println(msg);
    display.display();
}

void startup()
{
    // Clear the buffer.
    display.clearDisplay();
    // Draw bitmap on the screen
    display.drawBitmap(0, 0, wertteLogo, 128, 64, 1);
    display.display();
    delay(2000);
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE, BLACK);
    display.setCursor(0, 20);
    display.println("Device IP: ");
    display.println(WiFi.localIP());
    display.display();
    delay(3000);
    printStatus(true);
}

void dispOff()
{
    display.clearDisplay();
    display.display();
}

void printLDR(bool clear)
{
    if(clear)
    {
        display.clearDisplay();
        display.setTextSize(1);
        display.setTextColor(WHITE, BLACK);
        display.setCursor(30, 10);
        display.print("LDR Status");
        display.setCursor(0, 20);
        display.print("Dark: 4096");
        display.setCursor(0, 30);
        display.print("Bright: 0");
        display.setCursor(0, 40);
        display.print("Raw: ");
        display.setCursor(30, 40);
        display.print(analogRead(34));
        display.setCursor(0,50);
        display.print("Avg: ");
        display.setCursor(30,50);
        display.print(getLight());
        display.display();
    }
    else //Just update values
    {
        display.setTextColor(WHITE, BLACK);
        display.setCursor(30,50);
        display.print(getLight());
        display.setCursor(30, 40);
        display.print(analogRead(34));
        display.display();
    }
    
}

void printConInfo()
{
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE, BLACK);
    display.setCursor(0, 10);
    display.print("SSID: ");
    display.setCursor(50, 10);
    display.print(WIFI_SSID);
    display.setCursor(0, 20);
    display.print("PASS: ");
    display.setCursor(50, 20);
    display.print(WIFI_PASS);
    display.setCursor(0, 40);
    display.println("Device IP: ");
    display.println(WiFi.localIP());
    display.display();
}

void printDebugCon()
{
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE, BLACK);
    display.setCursor(0, 10);
    display.println("DEBUG IP: ");
    display.setCursor(0, 30);
    display.println(WiFi.localIP());
    display.display();
}
//TODO: Add display which relay is forced
//TODO2: Add empty space when writing < 10 numbers. Display cur shows 1 as 10 due to 0 not cleared
void printStatus(bool clear)
{
    if(clear)
    {
        display.clearDisplay();
        display.setTextSize(1);
        display.setTextColor(WHITE, BLACK);

        if(forcedAct == 0)
        {
            display.setCursor(0, 10);
            display.print("Relay 1: ");
            display.setCursor(50, 10);
            if(onTime1 < 1000) //3 chars and under
            {
                display.print(onTime1);
                display.setCursor(70, 10);
                display.print("/");
                display.setCursor(80, 10);
                display.print(offTime1);
            }
            else //+4 chars
            {
                display.print(onTime1);
                display.setCursor(75, 10);
                display.print("/");
                display.setCursor(85, 10);
                display.print(offTime1);
            }
            display.setCursor(0, 30);
            display.print("Relay 2: ");
            display.setCursor(50, 30);
            if(onTime2 < 1000) //3 chars and under
            {
                display.print(onTime2);
                display.setCursor(70, 30);
                display.print("/");
                display.setCursor(80, 30);
                display.print(offTime2);
            }
            else //+4 chars
            {
                display.print(onTime2);
                display.setCursor(75, 30);
                display.print("/");
                display.setCursor(85, 30);
                display.print(offTime2);
            }
        }
        else
        {
            display.setTextSize(2);
            display.print("FORCED");
            display.setTextSize(1);
        }
        
        display.setCursor(0, 50);
        display.print("Temp: ");
        display.setCursor(31, 50);
        display.print(curTemp);
        display.setCursor(63, 50);
        display.print("Hum: ");
        display.setCursor(90, 50);
        display.print(curHum);
        display.display();
    }
    else 
    {
        display.setTextColor(WHITE, BLACK);
        if(forcedAct == 0)
        {
            display.setCursor(50, 10);
            if(onTime1 < 1000) //3 chars and under
            {
                display.print(onTime1);
                display.setCursor(70, 10);
                display.print("/");
                display.setCursor(80, 10);
                display.print(offTime1);
            }
            else //+4 chars
            {
                display.print(onTime1);
                display.setCursor(75, 10);
                display.print("/");
                display.setCursor(85, 10);
                display.print(offTime1);
            }
            display.setCursor(50, 30);
            if(onTime2 < 1000) //3 chars and under
            {
                display.print(onTime2);
                display.setCursor(70, 30);
                display.print("/");
                display.setCursor(80, 30);
                display.print(offTime2);
            }
            else //+4 chars
            {
                display.print(onTime2);
                display.setCursor(75, 30);
                display.print("/");
                display.setCursor(85, 30);
                display.print(offTime2);
            }
        }
        else
        {
            display.setTextSize(2);
            display.print("FORCED");
            display.setTextSize(1);
        }
        
        display.setCursor(31, 50);
        display.print(curTemp);
        display.setCursor(90, 50);
        display.print(curHum);
        display.display();
    }
}
//FS helpers
String getFile(String fname)
{
    String reply = "";
    File file = SPIFFS.open(fname, "r"); 
    if(!file){ 
        Serial.println("Failed to open file for reading"); 
    }
    else //Read file
    {        
        Serial.print("Reading file ");
        Serial.println(fname);
        Serial.print("File size: ");
        Serial.println(file.size());
        uint8_t indexFile[4096] = {0}; //8192 seems to fail?
        file.read(indexFile, file.size());
        file.close();
        reply = (char*)indexFile;
    }
    return reply;
}
