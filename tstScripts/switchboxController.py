import socket
from tkinter import *
from tkinter import colorchooser
import datetime
import threading
import os.path
import json
from types import SimpleNamespace

DEV_NAME = "TMP"
MAIN_IP = "192.168.88.180"
UDP_PORT = 4200
ONR1 = 0
OFFR1 = 0
ONR2 = 0
OFFR2 = 0
SYSLOGIP = ""
NTPIP = ""
TEMP = 0.0
HUM = 0.0
LIGHT = 0

def report():
    global ONR1, ONR2, OFFR1, OFFR2, HUM, TEMP, LIGHT
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    #NICs need to be filtered, grabbed VM one by accident
    #hostname = socket.gethostname()
    #local_ip = socket.gethostbyname(hostname)
    local_ip = "192.168.88.200"
    local_port = 4200
    server_address = (local_ip, local_port)
    print(f"{local_ip}, {local_port}")
    s.bind(server_address)
    s.sendto(f"R:{local_ip}:{local_port}".encode('utf-8'), (MAIN_IP, UDP_PORT))
    print("Send data waiting for reply...")
    data, address = s.recvfrom(4096)
    print("Got reply!:")
    print(data)
    dataVars = data.decode().split(":")
    for item in dataVars:
        print(item)
    ONR1 = dataVars[0]
    ONR2 = dataVars[1]
    OFFR1 = dataVars[2]
    OFFR2 = dataVars[3]
    HUM = round(float(dataVars[4]), 2)
    TEMP = round(float(dataVars[5]), 2)
    LIGHT = dataVars[6]
    updateData()

def setValues():
    #NOTE! Remember to pad values. Ints need to be 5 chars long so leading 0 need to be added!
    global onT1Entry, onT2Entry, offT1Entry, offT2Entry
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.sendto(f"T:{onT1Entry.get()}:{offT1Entry.get()}:{onT2Entry.get()}:{offT2Entry.get()}".encode('utf-8'), (MAIN_IP, UDP_PORT))

def updateData():
    global onT1Label, onT2Label, offT1Label, offT2Label, tempLabel, humLabel, lightLabel
    onT1Label.config(text = f"Relay1 ON: {ONR1}")
    onT2Label.config(text = f"Relay2 ON: {ONR2}")
    offT1Label.config(text = f"Relay1 OFF: {OFFR1}")
    offT2Label.config(text = f"Relay2 OFF: {OFFR2}")
    tempLabel.config(text = f"TEMP: {TEMP}")
    humLabel.config(text = f"HUM: {HUM}")
    lightLabel.config(text = f"LIGHT: {LIGHT}")

def createUI():
    global ipcEntry, pcEntry, ipLabel, portLabel, autoVar, actScale, w, mainLVar, sideLVar, effEntry, nameLabel
    global onT1Label, onT2Label, offT1Label, offT2Label, tempLabel, humLabel, lightLabel, onT1Entry, onT2Entry, offT1Entry, offT2Entry
    #Info frame
    infoF = Frame(contF)
    frameLabel = Label(infoF, text="Device Info:", font='bold')
    nameLabel = Label(infoF, text=f"Name: {DEV_NAME}")
    ipLabel = Label(infoF, text=f"IP: {MAIN_IP}")
    portLabel = Label(infoF, text=f"Port: {UDP_PORT}")
    #Switchbox info
    onT1Label = Label(infoF, text=f"Relay1 ON: {ONR1}")
    offT1Label = Label(infoF, text=f"Relay1 OFF: {OFFR1}")
    onT2Label = Label(infoF, text=f"Relay2 ON: {ONR2}")
    offT2Label = Label(infoF, text=f"Relay2 OFF: {OFFR2}")
    syslogLabel = Label(infoF, text=f"Syslog IP: {SYSLOGIP}")
    ntpLabel = Label(infoF, text=f"NTP IP: {NTPIP}")
    tempLabel = Label(infoF, text=f"TEMP: {TEMP}")
    humLabel = Label(infoF, text=f"HUM: {HUM}")
    lightLabel = Label(infoF, text=f"LIGHT: {LIGHT}")
    onT1Label.grid(row=1, column=1, sticky="W")
    offT1Label.grid(row=2, column=1, sticky="W")
    onT2Label.grid(row=3, column=1, sticky="W")
    offT2Label.grid(row=4, column=1, sticky="W")
    syslogLabel.grid(row=5, column=1, sticky="W")
    ntpLabel.grid(row=6, column=1, sticky="W")
    tempLabel.grid(row=7, column=1, sticky="W")
    humLabel.grid(row=8, column=1, sticky="W")
    lightLabel.grid(row=9, column=1, sticky="W")
    frameLabel.grid(row=0)
    nameLabel.grid(row=1, sticky="W")
    ipLabel.grid(row=2, sticky="W")
    portLabel.grid(row=3, sticky="W")
    #Tst btn
    tstBtn = Button(infoF, text="TST", command=report)
    tstBtn.grid(row=4, sticky="W")
    #Config/force message
    actionsF = Frame(contF)
    frameLabel = Label(actionsF, text="Controls:", font='bold')
    onT1LabelAct = Label(actionsF, text=f"ON1 Time:")
    onT2LabelAct = Label(actionsF, text=f"ON2 Time:")
    offT1LabelAct = Label(actionsF, text=f"OFF1 Time:")
    offT2LabelAct = Label(actionsF, text=f"OFF2 Time:")
    onT1Entry = Entry(actionsF)
    onT2Entry = Entry(actionsF)
    offT1Entry = Entry(actionsF)
    offT2Entry = Entry(actionsF)
    frameLabel.grid(row=0, sticky="W")
    onT1LabelAct.grid(row=1, column=0, sticky="W")
    onT2LabelAct.grid(row=1, column=1, sticky="W")
    onT1Entry.grid(row=2, column=0, sticky="W")
    onT2Entry.grid(row=2, column=1, sticky="W")
    offT1LabelAct.grid(row=3, column=0, sticky="W")
    offT2LabelAct.grid(row=3, column=1, sticky="W")
    offT1Entry.grid(row=4, column=0, sticky="W")
    offT2Entry.grid(row=4, column=1, sticky="W")
    setBtn = Button(actionsF, text="Set Values", command=setValues)
    setBtn.grid(row=5)
    #Pack all
    infoF.grid(row=0)
    actionsF.grid(row=1)
    contF.pack()

root = Tk()
contF = Frame(root)
root.title("Static Controls")
createUI()
root.geometry("400x600")
root.mainloop()
