﻿# SwitchBox

SwitchBox project.

# Electronics

## Maximum ratings
Electrical characteristics are limited by relays used in SwitchBox.
Two SSR-40DA relays present are rated as follows:
Current: 40A
Voltage: 24-380VAC

# Operation

SwitchBox can be operated in headless mode through UDP command messages or by using web GUI.

## Relay Timing

Primary purpose of SwitchBox is to toggle it's relays on/off on specific intervals.
Relay timing can be controlled and configured through UDP or web interface.

Depending on usage and features present, shorter switching times should be favored when possible.
Enabling NTP is adviced for longer intervals due to reduced clock drift.

## Operation modes

SwitchBox has two operational modes.
1. Normal mode
2. Recovery mode
3. Debug mode

### Normal mode
In normal mode all standard and enabled functionality is present.
### Recovery mode
Recovery mode is used when WiFi credentials are changed or device needs to be reset. 
Device starts up as an WiFi accesspoint serving single web page allowing the resetting of WiFi credentials and settings.
Access point is named SB-RESET-AP. No password is required.

Recovery web page IP is:
1. 192.168.1.1
2. 192.168.4.1

NOTE! Recovery page IP depends on device settings.

Information about recovery mode is shown on LCD screen inside the SwitchBox.
Recovery mode can be entered by holding down screen change button during boot.

# User interfaces
## LCD
SwitchBox has an intergrated LCD display for debugging and status monitoring purposes,
### Status
Status screen shows current on/off times for both relays.
Readings of temperture and humidity sensor are shown on bottom of the screen.
### LDR
LDR screen shows current raw and filtered value of LDR sensor.
### Connection
Connection screen displays current IP address and SSID of connected network.
## Web pages
### Reset page
### Index page


# Features

## NTP

SwitchBox has NTP support which is used for syslog timestamp.
NTP can be enabled/disabled through settings.
In case no connection to NTP server can be made the SwitchBox defaults to using time since boot for relay timing. Syslog messages do not contain valid timestamps when operating without NTP.

## Syslog
Syslog messages are sent to configurable syslog server. Events including startup and periodic status messages are send.

## Sensoring
SwitchBox is designed to be used with DHT11/DHT22.
Both temperature and humidity data is read from the DHT sensor then filtered using average of 10 readings.

## UDP

UDP messages are used for simple control commands and status reply. See "Control Messages" section for commands.

## Control Messages

SwitchBox listens to commands on port 4200 when under normal operation.
Following commands can be used to operate SwitchBox:

|Message         |Command                        |Action                       |
|----------------|-------------------------------|-----------------------------|
|R				 |`R:IP:PORT`                    |Report status to IP + PORT   |
|F	             |`F:X:X`                    |Force relay state  (Relay 1/2 0=off 1=on)           |
|T               |`T:XXXXX:XXXXX:XXXXX:XXXXX`	 |Configure timing values. (on1/off1/on2/off2) Time in sec|



